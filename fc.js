document.getElementById("countButton").onclick = function() {  
    let typedText = document.getElementById("textInput").value;

typedText = typedText.toLowerCase();  
// This changes all the letter to lower case.  
typedText = typedText.replace(/[^a-z'\s]+/g, ""); 
let letterCounts = {}; 
for (let i = 0; i < typedText.length; i++) {
    currentLetter = typedText[i];
    // do something for each letter.
    

    if (letterCounts[currentLetter] === undefined) {
        letterCounts[currentLetter] = 1;  
    } else {  
        letterCounts[currentLetter]++;  
    }
 
    
}
for (let letter in letterCounts) {  
    let span = document.createElement("span");  
    let textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");  
    span.appendChild(textContent);  
    document.getElementById("lettersDiv").appendChild(span);  
}

let words = typedText.split(/\s/);
let wordCount={};
for (let i =0;i<words.length;i++){
    currentWord=words[i];
    if (wordCount[currentWord]=== undefined){
        wordCount[currentWord]=1;
    }   else {
        wordCount[currentWord]++;
    }
}
for (let word in wordCount) {  
    let span = document.createElement("span");  
    let textContent = document.createTextNode('"' + word + "\": " + wordCount[word] + ", ");  
    span.appendChild(textContent);  
    document.getElementById("wordsDiv").appendChild(span);  
}
}




